# Rédiger une fiche Rzine [<img src="https://rzine.fr/img/Rzine_logo.png"  align="right" width="120"/>](http://rzine.fr/)
### Modèle de mise en page, contenu attendu et soumission
**Comité éditorial Rzine**
<br/>  
-> [**Consulter la documentation**](https://rzine.gitpages.huma-num.fr/Help_Rzine_Layout)

<br/>  

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/4.0/)
